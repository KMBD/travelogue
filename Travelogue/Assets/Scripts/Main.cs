﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class Main : MonoBehaviour
{
    public TextMeshProUGUI txtDest;
    public GameObject txtStart;
    public GameObject txtStop;
    public List<GameObject> allLinesGO;
    public List<GameObject> greenLineGO;
    public List<GameObject> yellowLineGO;
    public List<GameObject> redLineGO;
    public List<GameObject> purpleLineGO;
    public List<GameObject> blueLineGO;
    public List<GameObject> brownLineGO;
    public Image imagePrev;
    public Image imageCurrent;
    public bool stop, allLine, greenOnly, yellowOnly, redOnly, purpleOnly, blueOnly, brownOnly;
    public int rnd;
    // Start is called before the first frame update
    void Start()
    {        
        greenLineGO = GameObject.FindGameObjectsWithTag("GreenLine").ToList();
        yellowLineGO = GameObject.FindGameObjectsWithTag("CircleLine").ToList();
        redLineGO = GameObject.FindGameObjectsWithTag("RedLine").ToList();
        purpleLineGO = GameObject.FindGameObjectsWithTag("PurpleLine").ToList();
        blueLineGO = GameObject.FindGameObjectsWithTag("BlueLine").ToList();
        brownLineGO = GameObject.FindGameObjectsWithTag("BrownLine").ToList();              
        InvokeRepeating("randomTimer", 0, 0.2f);
        imagePrev = null;
        allLine = true;
        greenOnly = false;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = false;        
        //Add all lines here one by one
        addAllLines();
    }

    // Update is called once per frame
    void Update()
    {                        
        if (!stop && allLine)
        {
            imageCurrent = allLinesGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }        
        else if(!stop && greenOnly)
        {
            imageCurrent = greenLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }
        else if(!stop && yellowOnly)
        {
            imageCurrent = yellowLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }
        else if (!stop && redOnly)
        {
            imageCurrent = redLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }
        else if (!stop && purpleOnly)
        {
            imageCurrent = purpleLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }
        else if (!stop && blueOnly)
        {
            imageCurrent = blueLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }
        else if (!stop && brownOnly)
        {
            imageCurrent = brownLineGO[rnd].gameObject.GetComponent<Image>();
            imageCurrent.color = new Color(255, 0, 0);
            txtDest.text = "You're Going To...\n" + imageCurrent.gameObject.name;
        }

    }
    public void addAllLines()
    {
        allLinesGO.AddRange(greenLineGO.ToList());
        allLinesGO.AddRange(yellowLineGO.ToList());
        allLinesGO.AddRange(redLineGO.ToList());
        allLinesGO.AddRange(purpleLineGO.ToList());
        allLinesGO.AddRange(blueLineGO.ToList());
        allLinesGO.AddRange(brownLineGO.ToList());

    }
    public void randomTimer()
    {
        try
        {
            if (allLine)
            {
                rnd = Random.Range(0, allLinesGO.Count);
                imagePrev = imageCurrent;                
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (greenOnly)
            {
                rnd = Random.Range(0, greenLineGO.Count);                
                imagePrev = imageCurrent;                
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (yellowOnly)
            {
                rnd = Random.Range(0, yellowLineGO.Count);
                imagePrev = imageCurrent;
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (redOnly)
            {
                rnd = Random.Range(0, redLineGO.Count);
                imagePrev = imageCurrent;
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (purpleOnly)
            {
                rnd = Random.Range(0, purpleLineGO.Count);
                imagePrev = imageCurrent;
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (blueOnly)
            {
                rnd = Random.Range(0, blueLineGO.Count);
                imagePrev = imageCurrent;
                imagePrev.color = new Color(255, 255, 255);
            }
            else if (brownOnly)
            {
                rnd = Random.Range(0, brownLineGO.Count);
                imagePrev = imageCurrent;
                imagePrev.color = new Color(255, 255, 255);
            }
        }
        catch(System.IndexOutOfRangeException ex)
        {

        }        
    }   

    IEnumerator waitForSeconds()
    {
        yield return new WaitForSeconds(0.1f);
    }

    public void btnPress()
    {
        if (!stop)
        {
            stop = true;
            StopAllCoroutines();
            CancelInvoke();
            txtStart.SetActive(true);
            txtStop.SetActive(false);
        }
        else
        {
            txtStart.SetActive(false);
            txtStop.SetActive(true);
            stop = false;
            InvokeRepeating("randomTimer", 0, 0.2f);            
        }
    }

    public void allLineTicked()
    {
        for(int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }        
        allLine = true;
        greenOnly = false;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = false;
    }

    public void greenLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = true;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = false;
    }
    
    public void yellowLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = false;
        yellowOnly = true;
        redOnly = false;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = false;
    }

    public void redLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = false;
        yellowOnly = false;
        redOnly = true;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = false;
    }
    public void purpleLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {            
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = false;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = true;
        blueOnly = false;
        brownOnly = false;
    }
    public void blueLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = false;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = false;
        blueOnly = true;
        brownOnly = false;
    }
    public void brownLineTicked()
    {
        for (int i = 0; i < allLinesGO.Count; i++)
        {
            Image changeImg = allLinesGO[i].gameObject.GetComponent<Image>();
            changeImg.color = new Color(255, 255, 255);
        }
        allLine = false;
        greenOnly = false;
        yellowOnly = false;
        redOnly = false;
        purpleOnly = false;
        blueOnly = false;
        brownOnly = true;
    }
}
